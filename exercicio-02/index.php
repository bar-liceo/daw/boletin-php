<html>
    <head>
        <title>Exercicio 2</title>
</head>
<body>
<h1>Exercicio 2</h1>
<p>
Crea un script en PHP que nos indique si el número X, que introducimos en un formulario es primo o no y que muestre por pantalla un mensaje tal que: “El número X es un número primo” o en caso de no serlo “El número X no es un número primo”
</p>

<hr/>

<?php
//print_r($_POST);

function textoSiPrimo ($n){
    if ($n == 0){
        return "El número 0 es una cosa rara XD.";
    }
    if ($n < 0){
        return "El número $n no es un número Natural.";
    }
    for ($i = 2; $i <= $n/2; $i++){
        if ($n % $i == 0){
            return "El número $n NO es un número primo.";
        }
    }

    return "El número $n es un número primo.";
}

if (isset($_POST["number"])){
    $number = $_POST["number"];

    $texto = textoSiPrimo($number);
}
?>

<div>
    <form action="index.php" method="post">
        <label for="number">Número:</label><br/>
        <input type="number" id="number"
        name="number" value="<?php
            if (isset($number)){
                print($number);
            } else{
                print(0);
            }
        ?>" /><br/>
        <input type="submit" value="Primo?" />
    </form>
</div>

<?php
    print("<p>$texto</p>");
?>
</body>
</html>