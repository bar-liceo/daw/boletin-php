# Boletín de PHP


Este é o boletín de PHP do módulo Despregamento de Aplicacións Web do ciclo de Desenvolvemento de Aplicacións Web.


## Exercicios

1. Crea un script en PHP que pida dos números por pantalla (X e Y) y los
compare indicando el menor con un mensaje por pantalla: "El número
más pequeño entre X e Y es NÚMEROMENOR".
2. Crea un script en PHP que nos indique si el número X, que introducimos
en un formulario es primo o no y que muestre por pantalla un mensaje
tal que: "El número X es un número primo" o en caso de no serlo "El
número X no es un número primo".
3. Crea un script en PHP que sume dos arrays de igual tamaño que pedirá
por pantalla y muestre su valor en una tabla vertical de tres columnas: la
primera con los datos del primer array, la segunda con los datos del
segundo y la tercera con la suma.
4. Escribe un script que pida 20 números por pantalla mediante un
formulario y que después muestre los números introducidos en vertical,
junto con las palabras máximo y mínimo al lado del máximo y el mínimo
respectivamente.
5. Realiza el control de acceso a una caja fuerte cuya combinación está
compuesta por cuatro cifras. Únicamente habrá cuatro intentos en un
formulario que se mostrará por pantalla, si acertamos se enviará un
mensaje de éxito por pantalla (en letras cuyo tamaño será 40 px y con un
color vivo) y si no un mensaje que diga "prueba de nuevo".
