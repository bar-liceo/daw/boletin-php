<html>
    <head>
        <title>Exercicio 4</title>
</head>
<body>
<h1>Exercicio 4</h1>
<p>
Escribe un script que pida 20 números por pantalla mediante un formulario y que después muestre los números introducidos en vertical, junto con las palabras máximo y mínimo al lado del máximo y el mínimo respectivamente.
</p>

<hr/>

<?php
    $tamanho = 20;
    print_r($_POST);

?>

<div>
    <form action="index.php" method="post">
        <?php
        for($i=1; $i<=$tamanho; $i++){
            if (isset($_POST["numero$i"])){
                $valor = $_POST["numero$i"];
            } else {
                $valor = 0;
            }

            print("
            <label for='numero$i'>Número$i</label><br/>
            <input type='number' id='numero$i'
                name='numero$i' value='$valor'/><br/>");
        }
        ?>
        
        <input type="submit" value="OK" />
    </form>
</div>

<table>
    <tr>
        <th>indice</th>
        <th>Array</th>
        <th>Max/Min</th>
    </tr>
    <?php
        if (isset($_POST["numero1"])){
            $numeros = array();
            $max=$_POST["numero1"];
            $min=$_POST["numero1"];
            
            for($i=1; $i<=$tamanho; $i++){
                $numeros[$i] = $_POST["numero$i"];
                if ($numeros[$i] > $max){
                    $max = $numeros[$i];
                }
                if ($numeros[$i] < $min){
                    $min = $numeros[$i];
                }
            }

            
            for($i=1; $i<= count($numeros); $i++){
                $texto = '';
                if ($numeros[$i] == $max){
                    $texto = " máximo ";
                }
                if ($numeros[$i] == $min){
                    $texto = " mínimo ";
                }
                print("<tr>
                <td>Número $i</td>
                <td>$numeros[$i]</td>
                <td>$texto</td>
                </tr>");
            }
        }
?>
</table>

</body>
</html>