<html>
    <head>
        <title>Exercicio 3</title>
</head>
<body>
<h1>Exercicio 3</h1>
<p>
Crea un script en PHP que sume dos arrays de igual tamaño que pedirá por pantalla y muestre su valor en una tabla vertical de tres columnas: la primera con los datos del primer array, la segunda con los datos del segundo y la tercera con la suma.
</p>

<hr/>

<?php
print_r($_POST);


if (isset($_POST["tamanho"])){
    $tamanho = $_POST["tamanho"];

    $array1 = array();
    $array2 = array();
    $suma = array();

}
?>

<div>
    <form action="index.php" method="post">
        <label for="tamanho">Tamaño:</label><br/>
        <input type="number" id="tamanho"
        name="tamanho" value="<?php
            if (isset($tamanho)){
                print($tamanho);
            } else{
                print(0);
            }
        ?>" /><br/>
        <input type="submit" value="OK" />
    </form>
</div>

<table>
    <tr>
        <th>Array1</th>
        <th>Array2</th>
        <th>Suma</th>
    </tr>
    <?php
    for($i=0; $i<$tamanho; $i++){
        $array1[$i] = rand(-100, 100);
        $array2[$i] = rand(-100, 100);

        $suma[$i] = $array1[$i] + $array2[$i];

        print("<tr>
        <td>$array1[$i]</td>
        <td>$array2[$i]</td>
        <td>$suma[$i]</td>
        </tr>");
    }
?>
    <tr>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>

</body>
</html>