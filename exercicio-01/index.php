<!DOCTYPE html>
<html>
 <head>
    <meta charset="utf-8">
    <title>Comparar Números</title>
 </head>
 <body>
    <h1>Exercicio 1: Compara números</h1>
    <p>Crea un script en PHP que pida dos números por pantalla (X e Y) y los compare indicando el menor con un mensaje por pantalla: "El número más pequeño entre X e Y es NÚMEROMENOR".</p>
    <hr/>
 <?php
    /*
    * Crea un script en PHP que pida dos números por pantalla (X e Y) y los
    * compare indicando el menor con un mensaje por pantalla: "El número
    * más pequeño entre X e Y es NÚMEROMENOR"
    * */

    function textoComparacion($numero1, $numero2){
        /**
         * Esta función recibe 2 números por parametro, compáraos e devolvenos o texto en HTML que queremos poñer como resultado da comparación.
         */
        $texto = '';
        if ($numero1 > $numero2){
            $texto ="
                <div>
                <p>El $numero1 es mayor que $numero2</p>
                </div>
            ";
        } else if ($numero2 > $numero1){
            $texto = "
                <div>
                <p>El $numero2 es mayor que $numero1</p>
                </div>
            ";
        } else{
            $texto = "
                <div>
                <p>El $numero1 es igual a $numero2</p>
                </div>
            ";
        }
        return $texto;
    }

    $texto = "";
    if (isset($_GET["numberX"])
        && isset($_GET["numberY"])){
            /* Revisamos se recibimos os parámetros do formulario.
            Así evitamos os warnings e controlamos se é a primeira vez que cargamos a páxina ou non. */

            $numberX = $_GET["numberX"];
            $numberY = $_GET["numberY"];
            $texto = textoComparacion($numberX, $numberY);
    }
    ?>

    <div>
        <form action="index.php" method="get">
            <label for="numberX">First number</label><br>
            <input type="number" id="numberX"
            name="numberX" value="<?php
            if (isset($numberX)){
                /*Se recibimos o parametro numberX do formulario témolo almacenado na variable $numberX. Entón mostramola no campo do formulario correspondente. Se non, non mostramos nada dentro do campo do formulario.*/
                echo($numberX);
            } 
            ?>"><br>

            <label for="numberY">Second number:</label><br>
            <input type="number" id="numberY"
            name="numberY" value="<?php
            if (isset($numberY)) {
                /*Se recibimos o parametro numberY do formulario témolo almacenado na variable $numberY. Entón mostramola no campo do formulario correspondente. Se non, non mostramos nada dentro do campo do formulario.*/
                echo($numberY);
            }
            ?>"><br><br>

            <input type="submit" value="Compare">
        </form>
    </div>
    <?php
        echo($texto);
    ?>
 </body>
</html>