<!DOCTYPE html>
<html lang="gl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercicio 5</title>
</head>
<body>
<h1>Exercicio 5</h1>
<p>
    Realiza el control de acceso a una caja fuerte cuya combinación está compuesta por cuatro cifras. Únicamente habrá cuatro intentos en un formulario que se mostrará por pantalla, si acertamos se enviará un mensaje de éxito por pantalla (en letras cuyo tamaño será 40 px y con un color vivo) y si no un mensaje que diga “prueba de nuevo”.
</p>

<hr/>

<?php
    $numero = "0000";

    if (isset($_POST["intentos"])){
        $intentos = $_POST["intentos"]-1;
    } else{
        $intentos = 4;
    }
    if (isset($_POST["clave"])){
        if ($intentos > 0){
            if ($numero == $_POST["clave"]){
                $aberto=1;
            }
        }
    }
?>

<form method="post" action="index.php">

    <label for='clave'>Clave:</label>
    <br/>
    <input type="text" id="clave" name="clave" minlength="4" maxlength="4" pattern="\d{4}" size="6"
    <?php
        if ($intentos<1 || isset($aberto)){
            print(" disabled ");
        }
    ?>
    />
    <input type="hidden" name="intentos" value="<?php
        print("$intentos");
    ?>" />
    <br/>
    <input type="submit" value="Validar"
    <?php
        if ($intentos<1 || isset($aberto)){
            print(" disabled ");
        }
    ?>
    />
</form>

<hr />

<?php
if(isset($_POST["intentos"])){
    if (isset($aberto)){
        print("<p style='color:red; font-size:40px;'>CAIXA ABERTA!</p>");
    } else {
        if ($intentos >0){
            print("<p>Proba de novo!</p>");
        } else {
            print("<p>Non se pode volver intentar. Caixa bloqueada!</p>");
        }
    }
}
?>

</body>
</html>


